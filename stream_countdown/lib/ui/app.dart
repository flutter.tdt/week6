import 'package:flutter/material.dart';
import '../blocs/count_down.dart';

class App extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Countdown',
      home: HomeScreen(),
    );
  }
  
}

class HomeScreen extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    return HomeState();
  }

}

class HomeState extends State<HomeScreen> {
  late TimerBLoC timerBloc;
  late int seconds;

  @override
  void initState() {
    timerBloc = TimerBLoC();
    seconds = timerBloc.seconds;
    timerBloc.countDown();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Container(
        child: StreamBuilder(
          stream: timerBloc.secondsStream,
          initialData: seconds,
          builder: (BuildContext context, AsyncSnapshot<dynamic> snapshot) {
            if (snapshot.hasError) {
              print(snapshot.error);
            }

            if (snapshot.hasData) {
              return Center(
                child: Text(snapshot.data.toString(),
                            style: TextStyle(fontSize: 100),
                ),
              );
            } else {
              return Center();
            }
          },
        )
      ),
    );
  }

}